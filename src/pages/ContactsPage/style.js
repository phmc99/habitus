import styled from "styled-components";

export const Container = styled.div`
  width: 90%;
  height: 100vh;

  ul {
    list-style: none;
    text-align: center;
    margin-top: 50px;
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly;
    gap: 20px;

    li {
      width: 280px;
    }
  }

  h1 {
    text-align: center;
    margin: 0;
    padding-top: 50px;
  }

  @media (min-width: 768px) {
    width: 1200px;
    margin: 0 auto;

    ul {
      margin-top: 70px;

      li {
        margin: 10px;
        width: 350px;
      }
    }
  }
`;
