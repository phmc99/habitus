import AppBar from "../../components/AppBar";
import ContactCard from "../../components/ContactCard";
import { Container } from "./style";

const Contacts = () => {
  return (
    <>
      <AppBar />
      <Container>
        <h1>Contate-nos</h1>
        <ul>
          <li>
            <ContactCard
              devName="Anthony Freitas Maciel"
              cargo="Quality Assurance"
              gitlab="Anthony07M"
              linkedin="anthony-freitas-66790a1b7"
            />
          </li>
          <li>
            <ContactCard
              devName="Mein Mieko Chang"
              cargo="Product Owner"
              gitlab="mein-chang"
              linkedin="meinmiekochang"
            />
          </li>
          <li>
            <ContactCard
              devName="Pedro Costa"
              cargo="Tech Leader"
              gitlab="phmc99"
              linkedin="phmc99"
            />
          </li>
          <li>
            <ContactCard
              devName="Rafael Monteiro de Oliveira"
              cargo="Quality Assurance"
              gitlab="raf2019"
              linkedin="rafael-oliveira-0a917981"
            />
          </li>
          <li>
            <ContactCard
              devName="Stefano Garrigó Ludvichak"
              cargo="Scrum Master"
              gitlab="stefanogl"
              linkedin="stefano-l-7a9b301b6"
            />
          </li>
        </ul>
      </Container>
    </>
  );
};

export default Contacts;
