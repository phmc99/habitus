import styled from "styled-components";
import Signin from "../../assets/signin.svg";
import { keyframes } from "styled-components";

const appearRight =  keyframes`
  0%{
    opacity: 0;
    transform: translateX(400px)
  }
  
  100%{
    opacity: 1;
    transform: translateX(0px);
  }
`


export const Container = styled.div`
  display: flex;

  h1 {
    color: var(--pink);
    text-align: center;
    margin-bottom: 30px;
    animation: ${appearRight} 2s normal;
  }
`;

export const ContainerRight = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  background-color: var(--white);
  display: flex;

  form{
    animation: ${appearRight} 1.5s normal;
    margin-bottom: 1rem;
  }

  @media (min-width: 768px) {
    width: 50vw;
  }
`;

export const Background = styled.div`
  display: none;

  @media (min-width: 768px) {
    display: block;
    height: 100vh;
    width: 50%;
    background: url(${Signin}) no-repeat center var(--purple);
    background-size: 38vw;
    flex: 1;
  }
`;
