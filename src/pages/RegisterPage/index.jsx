import GoTo from "../../components/GoTo";
import RegisterForm from "../../components/RegisterForm";
import { Container, ContainerRight, Background } from "./styles";

const RegisterPage = () => {
  return (
    <Container>
      <Background />
      <ContainerRight>
        <h1>Cadastro</h1>
        <RegisterForm />
        <GoTo message="login" />
      </ContainerRight>
    </Container>
  );
};

export default RegisterPage;
