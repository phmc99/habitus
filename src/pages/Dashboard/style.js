import styled from "styled-components";

export const DashboardPage = styled.div`
  height: 100vh;
  display: flex;
  align-items: center;
  flex-direction: column;

  .content {
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  ul {
    margin-top: 20px;
    padding: 10px;
    width: 100%;
    list-style: none;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    gap: 10px;

    @media (min-width: 768px) {
      flex-direction: row;
      flex-wrap: wrap;
      justify-content: center;
      margin-top: 50px;
    }
    li {
      width: 100%;
      max-width: 400px;
      margin: 10px;
    }
  }

  .spinner {
    margin-top: 30vh;
  }
`;
