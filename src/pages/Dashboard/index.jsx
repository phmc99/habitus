import { useState } from "react";
import AppBar from "../../components/AppBar";
import Card from "../../components/Card";
import FloatButton from "../../components/FloatButton";
import ModalHabit from "../../components/ModalHabit";
import ModalNewHabit from "../../components/ModalNewHabit";
import { DashboardPage } from "./style";
import { useHabitsGet } from "../../providers/HabitsGet";
import FadeLoader from "react-spinners/FadeLoader";

const Dashboard = () => {
  const [NewHabit, setNewHabit] = useState(false);
  const [HabitCard, setHabitCard] = useState(false);
  const [SelectedItem, setSelectedItem] = useState({});

  const handleCardClick = (item) => {
    setSelectedItem(item);
    setHabitCard(true);
  };

  const { habits, loading } = useHabitsGet();

  return (
    <>
      {NewHabit && <ModalNewHabit setModal={setNewHabit} />}
      {HabitCard && <ModalHabit item={SelectedItem} setModal={setHabitCard} />}
      <AppBar />
      <DashboardPage>
        <div className="content">
          <h1>Meus hábitos</h1>

          {loading ? (
            <div className="spinner">
              <FadeLoader
                loading={loading}
                size={150}
                color="var(--purple)"
                height={20}
                radius={5}
              />
            </div>
          ) : (
            <ul>
              {habits.map((data) => (
                <li key={data.id}>
                  <Card
                    name={data.title}
                    category={data.category}
                    onClick={() => handleCardClick(data)}
                  />
                </li>
              ))}
            </ul>
          )}
        </div>

        <FloatButton onClick={() => setNewHabit(true)} />
      </DashboardPage>
    </>
  );
};

export default Dashboard;
