import { Container, Background } from "./styled";
import logo from "../../assets/logo.png";
import ButtonAction from "../../components/ButtonAction";
import { useHistory } from "react-router-dom";

const Home = () => {
  const history = useHistory();

  const handleToRegister = () => history.push("/register");

  const handleToLogin = () => history.push("/login");

  return (
    <Container>
      <Background />
      <div className="content">
        <figure>
          <img src={logo} alt="main logo app" />
        </figure>
        <div className="text">
          <p>
            Melhore seus hábitos, crie novos!
            <br /> Conecte-se a outras pessoas!
            <br /> Viva com mais saúde e feliz!
          </p>
        </div>
        <div className="buttons-container">
          <ButtonAction
            className="button-shadow"
            purple
            onClick={handleToRegister}
          >
            Cadastro
          </ButtonAction>
          <ButtonAction onClick={handleToLogin}>Login</ButtonAction>
        </div>
      </div>
    </Container>
  );
};

export default Home;
