import styled from "styled-components";
import BackImage from "../../assets/background.png";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100vh;
  background-color: var(--white);

  figure {
    margin: 1rem;
  }

  img {
    width: 200px;
    border: 50%;
  }

  .text {
    width: 100%;
  }
  p {
    font-size: 1.3rem;
    margin: 1.1rem;
    text-align: center;
  }

  .buttons-container {
    width: 100%;
    display: flex;
    justify-content: center;
    gap: 40px;
    flex-direction: column;
    align-items: center;

    @media (min-width: 768px) {
      flex-direction: row;
    }
  }

  .content {
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 1.2rem;

    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  @media (min-width: 768px) {
    img {
      width: 500px;
      border-radius: 50%;
      box-shadow: 0px 7px 15px 3px rgb(0 0 0 / 20%);
    }

    p {
      font-size: 2rem;
      text-align: center;
    }

    .content {
      width: 80%;
    }

    .buttons-container {
      width: 80%;
      gap: 80px;
    }

    .buttons {
      display: flex;
      align-content: space-between;
    }

    button {
      box-shadow: 0px 11px 8px 0px rgb(0 0 0 / 20%);
      width: 200px;
      height: 60px;
      font-size: 1.6rem;
    }
  }
`;

export const Background = styled.div`
  background-image: url(${BackImage});
  background-size: cover;
  background-position: 50% 50%;
  width: 100vw;
  height: 100vh;
  transform: rotate(180deg);
`;
