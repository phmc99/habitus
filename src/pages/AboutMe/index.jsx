// import ModalUserProfile from "../ModalUserProfile";
import api from "../../services/api";
import AppBar from "../../components/AppBar";
import { Container } from "./styled";
import ButtonAction from "../../components/ButtonAction";
import UsersCard from "../../components/UsersCard";
import { useState } from "react";
import { useEffect } from "react";
import ModalUserProfile from "../../components/ModalUserProfile";

const AboutMe = () => {
  const [aboutMe, setAboutMe] = useState(false);
  const [userDetail, setUserDetail] = useState([]);
  const getUser = async () => {
    const token = await JSON.parse(localStorage.getItem("@habits:token"));
    const id = await JSON.parse(localStorage.getItem("@habits:id"));
    await api
      .get(
        `/users/${id}/`,
        {},
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        setUserDetail(response.data);
      });
  };
  useEffect(() => {
    getUser();
  }, [userDetail]);

  return (
    <>
      <AppBar />
      {aboutMe && <ModalUserProfile setModal={setAboutMe} />}
      <Container>
        <h1>Sobre mim</h1>
        <div className="card-design">
          <UsersCard item={userDetail} />
          <ButtonAction onClick={() => setAboutMe(true)}>
            Editar dados
          </ButtonAction>
        </div>
      </Container>
    </>
  );
};

export default AboutMe;
