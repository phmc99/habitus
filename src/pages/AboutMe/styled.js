import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  /* background-color: var(--pink); */
  .card-design {
    background-color: var(--violet);
    display: flex;
    flex-direction: column;
    /* justify-content: center; */
    align-items: center;

    width: 90vw;

    margin-top: 20px;

    padding: 40px 0 20px;

    border-radius: 10px;

    .div {
      width: 100%;
    }

    img {
      width: 100px;
    }

    p {
      margin-left: 50px;
      font-size: 2rem;
    }

    span {
      margin-left: 50px;
      font-size: 1.2rem;
    }

    @media (min-width: 768px) {
      width: 500px;

      margin-top: 70px;
    }
  }

  .spinner {
    margin-top: 30vh;
  }

  button {
    margin-top: 40px;
    width: 160px;
    color: var(--black);
  }
`;
