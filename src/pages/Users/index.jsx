import AppBar from "../../components/AppBar";
import UsersList from "../../components/UsersList";
import { useUsersGet } from "../../providers/UsersGet";
import { Container } from "./style";

import FadeLoader from "react-spinners/FadeLoader";

const Users = () => {
  const { loading } = useUsersGet();

  return (
    <>
      <AppBar />
      <Container>
        <h1>Outros usuários</h1>
        {loading ? (
          <div className="spinner">
            <FadeLoader
              loading={loading}
              size={150}
              color="var(--purple)"
              height={20}
              radius={5}
            />
          </div>
        ) : (
          <UsersList />
        )}
      </Container>
    </>
  );
};

export default Users;
