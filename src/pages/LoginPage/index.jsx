import FormLogin from "../../components/FormLogin";
import { Redirect } from "react-router";
import { useAuth } from "../../providers/Auth";
import { Container, Background } from "./styled";
import GoTo from "../../components/GoTo";

const PageLogin = () => {
  const { token } = useAuth();

  if (token) return <Redirect to="/dashboard" />;

  return (
    <Container>
      <div className="content">
        <h1>Login</h1>
        <FormLogin />
        <GoTo message="register" />
      </div>

      <Background />
    </Container>
  );
};
export default PageLogin;
