import styled from "styled-components";
import Login from "../../assets/login.svg";
import { keyframes } from "styled-components";

const appearLeft =  keyframes`
  0%{
    opacity: 0;
    transform: translateX(-400px)
  }
  
  100%{
    opacity: 1;
    transform: translateX(0px);
  }
`

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-evenly;
  height: 100vh;
  background: var(--white);

  h1 {
    margin-bottom: 20px;
  }

  form {
    width: 90%;
    padding: 5px;
    margin-bottom: 1rem;
  }

  form a {
    text-decoration: none;
    color: var(--pink);
    font-weight: bold;
    font-style: oblique;
  }

  form p {
    margin-top: 8px;
    font-size: 0.79rem;
    text-align: center;
  }

  .span-error {
    color: red;
    position: relative;
    top: -12px;
    left: 10px;
    font-size: 0.85rem;
  }

  .content {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 100%;
    max-width: 720px;
    text-align: center;
    animation: ${appearLeft} 1.5s normal;
  }

  @media (min-width: 768px) {
    .content {
      width: 50%;
    }

    form {
      width: 90%;
      max-width: 450px;
    }
    form p {
      font-size: 1rem;
    }
  }
`;

export const Background = styled.div`
  display: none;
  @media (min-width: 768px) {
    display: block;
    height: 100vh;
    width: 50%;
    background: url(${Login}) no-repeat center var(--purple);
    background-size: 38vw;
    flex: 1;
  }
`;
