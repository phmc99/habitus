import styled from "styled-components";

export const Container = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;

  gap: 20px;

  h1 {
    text-align: center;
    margin: 3rem 0rem;
    font-size: 3rem;
  }
`;

export const DescriptionBox = styled.div`
  width: 90%;
  border: 1px solid var(--grey);
  border-radius: 10px;
  padding: 0.75rem;
  background: var(--violet);
  box-shadow: 6px 6px 12px 3px var(--darkGrey);

  word-wrap: break-word;

  display: flex;
  justify-content: space-between;
  align-items: center;

  .edit-button {
    cursor: pointer;
    svg {
      font-size: 22px;
    }
  }
`;

export const GoalActivBox = styled.div`
  width: 90vw;

  .spinner {
    width: 90vw;
    background-color: var(--violet);
    border-radius: 10px;
    margin-bottom: 18px;
    gap: 10px;
    padding: 5px;
    box-shadow: 6px 6px 12px 3px var(--darkGrey);
    display: flex;
    align-items: center;
    flex-direction: column;

    @media (min-width: 900px) {
      width: 42vw;
    }

    h2 {
      margin-bottom: 2vh;
    }
  }

  @media (min-width: 900px) {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }
`;
