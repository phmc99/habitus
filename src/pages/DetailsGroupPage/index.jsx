import { Container, DescriptionBox, GoalActivBox } from "./styled";
import UserGroupDetails from "../../components/UserGroupDetails";
import { useUserGroupDetails } from "../../providers/UserGroupDetails";
import { useState } from "react";
import ModalNewActivity from "../../components/ModalNewActivity";
import ModalNewGoal from "../../components/ModalNewGoal";
import AppBar from "../../components/AppBar";
import { IoMdCreate } from "react-icons/io";
import ModalUpdateGroup from "../../components/ModalUpdateGroup";
import ModalUpdateGoal from "../../components/ModalUpdateGoal";
import ModalUpdateActivity from "../../components/ModalUpdateActivity";
import { useActivityCreate } from "../../providers/ActivityCreate";
import FadeLoader from "react-spinners/FadeLoader";
import { useGoalCreate } from "../../providers/GoalCreate";

const DetailsGroupPage = () => {
  const { detailsGroup } = useUserGroupDetails();

  const [NewActivity, setNewActivity] = useState(false);
  const [NewGoal, setNewGoal] = useState(false);

  const [UpdateActivity, setUpdateActivity] = useState(false);
  const [UpdateGoal, setUpdateGoal] = useState(false);

  const [UpdateGroup, setUpdateGroup] = useState(false);

  const { loadingA } = useActivityCreate();
  const { loadingG } = useGoalCreate();

  return (
    <>
      {NewActivity && <ModalNewActivity setModal={setNewActivity} />}
      {NewGoal && <ModalNewGoal setModal={setNewGoal} />}
      {UpdateGroup && <ModalUpdateGroup setModal={setUpdateGroup} />}

      {UpdateActivity && <ModalUpdateActivity setModal={setUpdateActivity} />}
      {UpdateGoal && <ModalUpdateGoal setModal={setUpdateGoal} />}

      <AppBar />
      <Container>
        <h1>{detailsGroup.name}</h1>
        <DescriptionBox>
          <div>
            <p>
              <strong>Categoria:</strong>
              &nbsp; {detailsGroup.category}
            </p>
            <p>
              <strong>Descrição:</strong>
              &nbsp; {detailsGroup.description}
            </p>
          </div>

          <div className="edit-button" onClick={() => setUpdateGroup(true)}>
            <span>
              <IoMdCreate />
            </span>
          </div>
        </DescriptionBox>

        <GoalActivBox>
          {loadingG ? (
            <div className="spinner">
              <h2>Metas</h2>
              <FadeLoader
                loading={loadingG}
                size={150}
                color="var(--purple)"
                height={20}
                radius={5}
              />
            </div>
          ) : (
            <UserGroupDetails
              componentType="goals"
              setCreateModal={setNewGoal}
              setUpdateModal={setUpdateGoal}
            />
          )}

          {loadingA ? (
            <div className="spinner">
              <h2>Atividades</h2>
              <FadeLoader
                loading={loadingA}
                size={150}
                color="var(--purple)"
                height={20}
                radius={5}
              />
            </div>
          ) : (
            <UserGroupDetails
              componentType="activities"
              setCreateModal={setNewActivity}
              setUpdateModal={setUpdateActivity}
            />
          )}
        </GoalActivBox>
      </Container>
    </>
  );
};

export default DetailsGroupPage;
