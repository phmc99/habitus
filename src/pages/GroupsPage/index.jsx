import { useState } from "react";
import AppBar from "../../components/AppBar";
import FloatButton from "../../components/FloatButton";
import ModalNewGroup from "../../components/ModalNewGroup";
import { GroupContent } from "./style";
import ModalGroupDetail from "../../components/ModalGroupDetail";
import Card from "../../components/Card";
import { useGroups } from "../../providers/GroupsGet";
import GroupSearch from "../../components/GroupSearch";
import { FiChevronsLeft, FiChevronsRight } from "react-icons/fi";
import { Button } from "../../components/UsersList/style";
import FadeLoader from "react-spinners/FadeLoader";

const GroupsPage = () => {
  const [NewGroup, setNewGroup] = useState(false);
  const [GroupInfo, setGroupInfo] = useState(false);

  const [SelectedItem, setSelectedItem] = useState({});

  const { allGroups, previousPage, nextPage, next, last, loading } =
    useGroups();

  const handleClick = (item) => {
    setSelectedItem(item);
    setGroupInfo(true);
  };

  return (
    <>
      {GroupInfo && (
        <ModalGroupDetail item={SelectedItem} setModal={setGroupInfo} />
      )}
      {NewGroup && <ModalNewGroup setModal={setNewGroup} />}
      <AppBar />
      <GroupContent>
        <div className="content">
          <h1>Comunidade</h1>
          <GroupSearch handleClick={handleClick} />

          {loading ? (
            <div className="spinner">
              <FadeLoader
                loading={loading}
                size={150}
                color="var(--purple)"
                height={20}
                radius={5}
              />
            </div>
          ) : (
            <>
              <ul>
                {allGroups.map((item, index) => (
                  <li key={index}>
                    <Card
                      name={item.name}
                      category={item.category}
                      onClick={() => handleClick(item)}
                    />
                  </li>
                ))}
              </ul>
              <div className="Buttons">
                <Button disabled={next === 1} onClick={previousPage}>
                  <FiChevronsLeft />
                </Button>
                <span>{next}</span>
                <Button disabled={last === null} onClick={nextPage}>
                  <FiChevronsRight />
                </Button>
              </div>
            </>
          )}
        </div>
        <FloatButton onClick={() => setNewGroup(true)} />
      </GroupContent>
    </>
  );
};

export default GroupsPage;
