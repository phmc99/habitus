import styled from "styled-components";

export const GroupContent = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;

  .content {
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  ul {
    margin-top: 20px;
    padding: 10px;
    width: 100%;
    list-style: none;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    gap: 20px;

    @media (min-width: 768px) {
      flex-direction: row;
      flex-wrap: wrap;
      justify-content: center;
    }

    li {
      width: 90%;
      max-width: 400px;
      margin: 10px;
    }
  }

  .Buttons {
    display: flex;
    align-items: center;
    margin-top: 20px;

    span {
      font-size: 1.5rem;
      color: var(--purple);
    }
  }

  .spinner {
    margin-top: 30vh;
  }
`;
