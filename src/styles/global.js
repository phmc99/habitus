import { createGlobalStyle } from "styled-components";
import wave from "../assets/background.png";

export const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  :root {
    --purple: #2E2249;
    --pink: #F64A8A;
    --white: #FFF6FA;
    --violet: #DFCBDB;
    --black: rgb(26,26,26);
    --grey:#C7C7C7;
    --border: 6px;
    --darkGrey: rgb(150 150 150);
  }

  ::-webkit-scrollbar{
    width: 12px;
    height: 12px;
  }
  ::-webkit-scrollbar-thumb{
    background: var(--pink);
    border-radius: 12px;
  }
  ::-webkit-scrollbar-thumb:hover{
    background: linear-gradient(90deg, var(--purple) 0%, var(--pink) 100%);;
  }
  ::-webkit-scrollbar-track{
    background: var(--grey);
    border-radius: 0px;
    box-shadow: inset 0px 0px 0px 0px #F0F0F0;
  }

  body {
    font-family: 'K2D', sans-serif;
    font-weight: 400;
    color: var(--black);
    background-color: var(--white);
    overflow-x: hidden;

    @media (min-width: 425px) {
      background-image: url(${wave});
      background-size: cover;
      background-position: 50% 50%;
    }
  }

  h1  {
    font-family: 'Coiny', cursive;
    color: var(--pink);
    margin-top: 50px;
    font-size: 2rem;
    text-shadow: 6px 5px 7px rgba(150, 150, 150, 1);

    @media (min-width: 500px) {
      font-size: 3rem;
    }
  }
  
  h2 {
    font-family: 'K2D', sans-serif;
    font-weight: 600;
    text-align: center;
    font-size: 1.5rem;

    @media (min-width: 500px) {
      font-size: 2rem;
    }
  }

  h3 {
    font-size: 1.3rem;
  }

  p{
    font-size: 1.1rem;
  }
  
  button{
    font-family: 'K2D', sans-serif;
    outline: none;
  }
  
  button:hover{
    transition: all 0.3s; 
    filter: brightness(70%); 
  }

  input, select {
    outline: none;
    font-family: 'K2D', sans-serif;
  }

  .span-error {
    color: red;
    font-size: .8rem;
    width: 100%;
  }

  .menu-closed-desktop {
        display: none;
        }

  @media (min-width: 768px) {
    .menu-closed {
      width: 50px;
      height: 100vh;
      background-color: var(--purple);
      position: fixed;
      cursor: pointer;
      color: var(--white);

      
      .menu-closed-mobile {
        display: none;
      }
      
      .menu-closed-desktop {
        display: flex;
        flex-direction: column;
        justify-content: space-between;

        height: 100%;

        padding: 10px;

        .menu-footer {
          align-self: flex-end;
        }
        svg {
          font-size: 28px;
        }
      }
    }
  }


`;
