import { createContext, useContext, useEffect, useState } from "react";
import api from "../../services/api";

export const UserGroupDetailsContext = createContext();

export const UserGroupDetailsProvider = ({ children }) => {
  const [detailsGroup, setDetailsGroup] = useState([]);
  const [groupId, setGroupId] = useState("");

  useEffect(() => {
    const getGroupInfo = async (id) => {
      await api.get(`/groups/${id}/`).then((resp) => {
        setDetailsGroup(resp.data);
      });
    };
    getGroupInfo(groupId);
  }, [groupId, detailsGroup]);

  return (
    <UserGroupDetailsContext.Provider
      value={{ detailsGroup, setGroupId, groupId }}
    >
      {children}
    </UserGroupDetailsContext.Provider>
  );
};

export const useUserGroupDetails = () => useContext(UserGroupDetailsContext);
