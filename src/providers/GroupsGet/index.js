import { createContext, useContext, useEffect, useState } from "react";
import toast from "react-hot-toast";
import api from "../../services/api";

export const GroupsContext = createContext();

export const GroupsProvider = ({ children }) => {
  const [allGroups, setAllGroups] = useState([]);
  const token = JSON.parse(localStorage.getItem("@habits:token"));

  const [next, setNext] = useState(1);
  const [last, setLast] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    api
      .get(`/groups/?page=${next}`)
      .then((response) => {
        setAllGroups(response.data.results);
        setLast(response.data.next);
        setLoading(false);
      })
      .catch((err) => toast.error(err));
  }, [token, next]);

  const previousPage = () => {
    setNext(next - 1);
  };

  const nextPage = () => {
    setNext(next + 1);
  };

  return (
    <GroupsContext.Provider
      value={{ allGroups, previousPage, nextPage, next, last, loading }}
    >
      {children}
    </GroupsContext.Provider>
  );
};

export const useGroups = () => useContext(GroupsContext);
