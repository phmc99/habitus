import { createContext, useContext, useState } from "react";
import toast from "react-hot-toast";
import api from "../../services/api";

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const token = JSON.parse(localStorage.getItem("@habits:token")) || "";
  const [authenticated, setAuthenticated] = useState(token);

  const signIn = async (user, history) => {
    await api
      .post("/sessions/", user)
      .then((response) => {
        const token = response.data.access;
        const { user_id } = JSON.parse(atob(token.split(".")[1]));

        setAuthenticated(token);

        localStorage.setItem("@habits:token", JSON.stringify(token));
        localStorage.setItem("@habits:id", JSON.stringify(user_id));

        toast.success("Seja Bem vindo");
      })
      .catch((_) => toast.error("Email ou senha incorretos"));
    history.push("/dashboard");
  };

  return (
    <AuthContext.Provider
      value={{ token: authenticated, signIn, setAuthenticated }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
