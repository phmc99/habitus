import { createContext, useContext, useEffect, useState } from "react";
import api from "../../services/api";
import { useAuth } from "../Auth";

const UsersGetContext = createContext();

export const UsersGetProvider = ({ children }) => {
  const [users, setUsers] = useState([]);
  const [next, setNext] = useState(1);
  const [last, setLast] = useState("");
  const [loading, setLoading] = useState(false);

  const { token } = useAuth();

  useEffect(() => {
    const getUsers = async () => {
      setLoading(true);
      await api
        .get(`users/?page=${next}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((response) => {
          setUsers(response.data.results);
          setLast(response.data.next);
          setLoading(false);
        });
    };
    getUsers();
  }, [token, next]);

  const previousPage = () => {
    setNext(next - 1);
  };

  const nextPage = () => {
    setNext(next + 1);
  };

  return (
    <UsersGetContext.Provider
      value={{ users, previousPage, nextPage, next, last, loading }}
    >
      {children}
    </UsersGetContext.Provider>
  );
};

export const useUsersGet = () => useContext(UsersGetContext);
