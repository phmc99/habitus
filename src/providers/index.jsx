import { ActivityCreateProvider } from "./ActivityCreate";
import { AuthProvider } from "./Auth";
import { GoalsCreateProvider } from "./GoalCreate";
import { GroupsProvider } from "./GroupsGet";
import { HabitsCreateProvider } from "./HabitsCreate";
import { HabitsGetProvider } from "./HabitsGet";
import { UserGroupDetailsProvider } from "./UserGroupDetails";
import { UsersGetProvider } from "./UsersGet";

const Providers = ({ children }) => {
  return (
    <AuthProvider>
      <GroupsProvider>
        <ActivityCreateProvider>
          <GoalsCreateProvider>
            <UserGroupDetailsProvider>
              <HabitsCreateProvider>
                <HabitsGetProvider>
                  <UsersGetProvider>{children}</UsersGetProvider>
                </HabitsGetProvider>
              </HabitsCreateProvider>
            </UserGroupDetailsProvider>
          </GoalsCreateProvider>
        </ActivityCreateProvider>
      </GroupsProvider>
    </AuthProvider>
  );
};

export default Providers;
