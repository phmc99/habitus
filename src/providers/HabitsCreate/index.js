import { createContext, useContext } from "react";
import toast from "react-hot-toast";
import api from "../../services/api";
import { useAuth } from "../Auth";

const HabitsCreateContext = createContext();

export const HabitsCreateProvider = ({ children }) => {
  const { token } = useAuth();

  const postHabits = (data) => {
    api
      .post("/habits/", data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => toast.success("Hábito adicionado!"))
      .catch(() => toast.error("Ops, algo deu errado!"));
  };

  return (
    <HabitsCreateContext.Provider value={{ postHabits }}>
      {children}
    </HabitsCreateContext.Provider>
  );
};

export const useHabitsCreate = () => useContext(HabitsCreateContext);
