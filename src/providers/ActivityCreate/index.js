import { createContext, useContext, useState } from "react";
import toast from "react-hot-toast";
import api from "../../services/api";
import { useAuth } from "../Auth";

const ActivityCreateContext = createContext();

export const ActivityCreateProvider = ({ children }) => {
  const [loadingA, setLoadingA] = useState(false);
  const { token } = useAuth();

  const postActivity = async (data) => {
    setLoadingA(true);
    await api
      .post("/activities/", data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => {
        toast.success("Atividade adicionada!");
        setLoadingA(false);
      })
      .catch(() => toast.error("Ops, algo deu errado!"));
  };

  return (
    <ActivityCreateContext.Provider value={{ postActivity, loadingA }}>
      {children}
    </ActivityCreateContext.Provider>
  );
};

export const useActivityCreate = () => useContext(ActivityCreateContext);
