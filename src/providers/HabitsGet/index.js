import { createContext, useContext, useEffect, useState } from "react";
import api from "../../services/api";
import { useAuth } from "../Auth";

export const HabitsGetContext = createContext();

export const HabitsGetProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);
  const [habits, setHabits] = useState([]);

  const { token } = useAuth();

  useEffect(() => {
    const getHabits = async () => {
      setLoading(true);
      await api
        .get("habits/personal/", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((response) => {
          setLoading(false);
        });
    };
    getHabits();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);

  useEffect(() => {
    const getHabits = async () => {
      await api
        .get("habits/personal/", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((response) => {
          setHabits(response.data);
        });
    };
    getHabits();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [habits, token]);

  useEffect(() => {});

  return (
    <HabitsGetContext.Provider value={{ habits, loading }}>
      {children}
    </HabitsGetContext.Provider>
  );
};

export const useHabitsGet = () => useContext(HabitsGetContext);
