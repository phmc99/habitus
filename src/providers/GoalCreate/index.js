import { createContext, useContext, useState } from "react";
import toast from "react-hot-toast";
import api from "../../services/api";
import { useAuth } from "../Auth";

const GoalsCreateContext = createContext();

export const GoalsCreateProvider = ({ children }) => {
  const [loadingG, setLoadingG] = useState(false);
  const { token } = useAuth();

  const postGoals = async (data) => {
    setLoadingG(true);
    await api
      .post("/goals/", data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => {
        toast.success("Meta adicionada!");
        setLoadingG(false);
      })
      .catch(() => toast.error("Ops, algo deu errado!"));
  };

  return (
    <GoalsCreateContext.Provider value={{ postGoals, loadingG }}>
      {children}
    </GoalsCreateContext.Provider>
  );
};

export const useGoalCreate = () => useContext(GoalsCreateContext);
