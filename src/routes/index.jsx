import { Switch, Route, Redirect } from "react-router-dom";
import Dashboard from "../pages/Dashboard";
import GroupsPage from "../pages/GroupsPage";
import Home from "../pages/HomePage";
import PageLogin from "../pages/LoginPage";
import RegisterPage from "../pages/RegisterPage";
import Details from "../pages/DetailsGroupPage";
import { useAuth } from "../providers/Auth";
import Contacts from "../pages/ContactsPage";
import Users from "../pages/Users";
import AboutMe from "../pages/AboutMe";

const Routes = () => {
  const { token } = useAuth();

  const CustomRoute = ({ component: Component, ...rest }) => {
    return (
      <Route
        {...rest}
        render={() => {
          return token ? <Component /> : <Redirect to={"/"} />;
        }}
      />
    );
  };

  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>

      <CustomRoute component={Dashboard} path="/dashboard" />
      <CustomRoute component={Details} path="/group/:id" />

      <Route path="/login">
        <PageLogin />
      </Route>

      <Route path="/register">
        <RegisterPage />
      </Route>

      <Route path="/groups">
        <GroupsPage />
      </Route>

      <Route path="/contacts">
        <Contacts />
      </Route>

      <Route path="/users">
        <Users />
      </Route>

      <Route path="/about">
        <AboutMe />
      </Route>
    </Switch>
  );
};

export default Routes;
