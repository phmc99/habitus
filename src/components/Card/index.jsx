import ButtonAction from "../ButtonAction";
import { CardBox } from "./style";

const Card = ({ name, category, ...rest }) => {
  return (
    <CardBox>
      <h2>{name}</h2>
      <span>categoria: {category}</span>
      <ButtonAction {...rest}>Veja mais</ButtonAction>
    </CardBox>
  );
};

export default Card;
