import styled from "styled-components";

export const CardBox = styled.div`
  width: 100%;
  height: 190px;
  background-color: var(--violet);
  border-radius: 10px;
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  box-shadow: 6px 6px 12px 3px var(--darkGrey);

  h2 {
    font-size: 1.5rem;
  }

  span {
    margin-top: 10px;
    font-size: 1rem;
  }
`;
