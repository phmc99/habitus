import styled from "styled-components";

export const NewGroupForm = styled.div`
  width: 70vw;
  max-width: 450px;

  form {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
  }

  label {
    margin: 25px 0 8px;
    display: block;
    font-size: 1.1rem;
  }

  select {
    padding: 0.7rem;
    border-radius: 10px;
    border: 0;
    width: 100%;
  }

  .container-input {
    background: var(--purple);

    svg {
      color: var(--white);
    }
  }

  input,
  select {
    background: var(--purple);
    color: var(--white);
    font-size: 1rem;
  }

  option {
    color: var(--black);
    border-radius: 100px;
    background: var(--violet);
  }
`;
