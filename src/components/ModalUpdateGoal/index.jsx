import ButtonAction from "../ButtonAction";
import { Input } from "../Input";
import ModalBase from "../ModalBase";
import { NewGroupForm } from "./style";
import { IoIosStats, IoMdPaper, IoMdQuote } from "react-icons/io";

import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../services/api";
import toast from "react-hot-toast";

const ModalUpdateGoal = ({ setModal }) => {
  const token = JSON.parse(localStorage.getItem("@habits:token"));
  const id = JSON.parse(localStorage.getItem("@habits:item-id"));

  const schemaGoal = yup.object().shape({
    title: yup.string(),
    difficulty: yup.string(),
    how_much_achieved: yup.number(),
    achieved: yup.boolean(),
  });

  const { register, handleSubmit } = useForm({
    resolver: yupResolver(schemaGoal),
  });

  const onSubmit = async (data) => {
    await api
      .patch(`/goals/${id}/`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => {
        toast.success("Meta atualizada!");
      })
      .catch(() => toast.error("Ops, algo deu errado!"));

    setModal(false);
  };

  const deleteItem = async () => {
    await api
      .delete(`/goals/${id}/`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => {
        toast.success("A meta foi excluida!");
      })
      .catch(() => toast.error("Ops, algo deu errado!"));

    setModal(false);
  };

  return (
    <ModalBase closeModal={setModal}>
      <NewGroupForm>
        <h2>Atualize a meta</h2>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            label="Titulo"
            icon={IoMdQuote}
            placeholder="Novo titulo da meta"
            {...register("title")}
          />

          <Input
            label={"Dificuldade"}
            icon={IoMdPaper}
            placeholder={`Nova dificuldade da meta`}
            {...register("difficulty")}
          />

          <Input
            label="Quanto que já progrediu?"
            placeholder="Progresso"
            type="number"
            min="0"
            max="100"
            icon={IoIosStats}
            {...register("how_much_achieved")}
          />

          <label>Atingiu seu objetivo?</label>
          <select {...register("achieved")}>
            <option value={true}>Sim</option>
            <option selected value={false}>
              Não
            </option>
          </select>

          <ButtonAction type="submit">Atualizar</ButtonAction>
        </form>
        <ButtonAction onClick={deleteItem}>Deletar</ButtonAction>
      </NewGroupForm>
    </ModalBase>
  );
};

export default ModalUpdateGoal;
