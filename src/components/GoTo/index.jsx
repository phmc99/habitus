import { SpanGoTo, StyledLink } from "./styles";

const GoTo = ({ message }) => {
  return (
    <SpanGoTo>
      {message === "login" ? "Já" : "Não"} tem uma conta? Faça seu
      <StyledLink to={message === "login" ? "/login" : "/register"}>
        {message === "login" ? " Login" : " Cadastro"}
      </StyledLink>
    </SpanGoTo>
  );
};

export default GoTo;
