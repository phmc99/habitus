import styled from "styled-components";
import { Link } from "react-router-dom";

export const SpanGoTo = styled.span`
  text-decoration: none;
  margin-bottom: 6rem;
  `;

export const StyledLink = styled(Link)`
  color: var(--pink);
  font-weight: 600;
  text-decoration: none;
  margin-left: 2px;

  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }
`;
