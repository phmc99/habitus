import { IoIosAdd } from "react-icons/io";
import { Details, RoundedButton } from "./styles";
import UserGroupDetailsCard from "../UserGroupDetailsCard";
import { useEffect, useState } from "react";
import { useUserGroupDetails } from "../../providers/UserGroupDetails";
import api from "../../services/api";

const UserGroupDetails = ({
  componentType,
  setCreateModal,
  setUpdateModal,
}) => {
  const [DetailsGroup, setDetailsGroup] = useState([]);

  const [type, setType] = useState("");

  const { detailsGroup, groupId } = useUserGroupDetails();

  useEffect(() => {
    if (componentType === "goals") {
      setType("goals");
    } else {
      setType("activities");
    }

    const getItems = async () => {
      await api
        .get(`/${componentType}/?group=${groupId}&page=1`)
        .then((resp) => setDetailsGroup(resp.data.results));
    };
    getItems();
  }, [componentType, groupId, detailsGroup]);

  return (
    <Details type={componentType}>
      <h2>{componentType === "goals" ? "Metas" : "Atividades"}</h2>

      <ul>
        {DetailsGroup &&
          DetailsGroup.map((item, index) => (
            <UserGroupDetailsCard
              item={item}
              type={type}
              key={index}
              setUpdateModal={setUpdateModal}
            />
          ))}
      </ul>
      <RoundedButton type={type} onClick={() => setCreateModal(true)}>
        <IoIosAdd />
      </RoundedButton>
    </Details>
  );
};

export default UserGroupDetails;
