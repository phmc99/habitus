import styled from "styled-components";
import scroll from "../../assets/scroll.png";

export const Details = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 470px;

  width: 90vw;
  background-color: var(--violet);
  border-radius: 10px;
  margin-bottom: 18px;
  gap: 10px;
  padding: 5px;
  box-shadow: 6px 6px 12px 3px var(--darkGrey);

  @media (min-width: 900px) {
    width: 42vw;
  }

  h2 {
    margin-bottom: 2vh;
  }

  ul {
    display: flex;
    flex-direction: column;
    gap: 10px;
    list-style: none;
    height: 80%;
    overflow-y: scroll;
    cursor: url(${scroll}), auto;
    ::-webkit-scrollbar {
      display: none;
    }

    .activities:nth-child(odd) {
      background-color: var(--purple);
      color: var(--white);
    }

    .goals:nth-child(even) {
      background-color: var(--purple);
      color: var(--white);
    }

    .achieved {
      /* filter: grayscale(2); */
      filter: opacity(0.2);
    }
  }
`;

export const RoundedButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  width: 40px;
  height: 40px;
  border-radius: 50%;
  background-color: ${(props) =>
    props.type === "goals" ? `var(--pink)` : `var(--purple)`};
  color: var(--white);
  border: none;
  margin-top: 10px;

  padding: 8px;
  cursor: pointer;

  svg {
    font-size: 1.5rem;
  }
`;
