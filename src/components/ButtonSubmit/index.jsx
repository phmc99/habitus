import { Container } from "./styles";

const ButtonSubmit = ({ children, ...rest }) => {
  return (
    <Container>
      <button {...rest}> {children}</button>
    </Container>
  );
};

export default ButtonSubmit;
