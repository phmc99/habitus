import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  margin-top: 1.2rem;

  button {
    width: 100%;
    background: var(--violet);
    border-radius: 10px;
    padding: 0.75rem;
    border: 0;
    font-size: 1rem;
    font-weight: bold;
  }

  button:hover {
    background: var(--pink);
    color: var(--white);
    transition: all 0.5s;
    cursor: pointer;
  }
`;
