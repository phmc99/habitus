import styled from "styled-components";

export const HabitContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 50px;
  min-width: 300px;
  width: 45vw;
  max-width: 400px;

  h2 {
    margin-bottom: 15px;
  }

  .header {
    text-align: center;
  }

  ul {
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 10px;

    list-style: none;
  }

  li {
    font-size: 1.2rem;
  }

  .footer {
    display: flex;
    gap: 20px;
  }
`;
