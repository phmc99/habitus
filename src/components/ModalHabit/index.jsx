import React from "react";
import ButtonAction from "../ButtonAction";
import ModalBase from "../ModalBase";
import { HabitContent } from "./style";
import api from "../../services/api";
import { useAuth } from "../../providers/Auth";
import toast from "react-hot-toast";
import FormMyHabit from "../../components/FormMyHabit";
import { useState } from "react";

const ModalHabit = ({ item, setModal }) => {
  const { token } = useAuth();
  const [showModal, setShowModal] = useState(false);

  const handleDeleteHabit = async (id) => {
    await api.delete(`/habits/${id}/`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    setModal(false);
    toast.success("Hábito deletado com sucesso");
  };

  const handle = () => {
    setShowModal(true);
  };
  return (
    <>
      <ModalBase closeModal={setModal}>
        {showModal && (
          <FormMyHabit
            habit={item}
            setClose={setShowModal}
            setModal={setModal}
          />
        )}
        <HabitContent>
          <div className="header">
            <h2>{item.title}</h2>
            <h3>{item.category}</h3>
          </div>
          <ul>
            <li>
              <strong>Dificuldade:</strong> {item.difficulty}
            </li>
            <li>
              <strong>Frequência:</strong> {item.frequency}
            </li>
            <li>
              <strong>Alcançado:</strong> {item.achived ? "sim" : "não"}
            </li>
            <li>{item.how_much_achieved}%</li>
          </ul>
          <div className="footer">
            <ButtonAction onClick={handle}>Atualizar</ButtonAction>
            <ButtonAction onClick={() => handleDeleteHabit(item.id)}>
              Remover
            </ButtonAction>
          </div>
        </HabitContent>
      </ModalBase>
    </>
  );
};

export default ModalHabit;
