import styled from "styled-components";

export const ContainerDate = styled.div`
  width: 100%;

  span {
    margin-top: 25px;
    display: block;
    font-size: 1.1rem;
  }
`;

export const ContentInputDate = styled.div`
  display: flex;
  align-items: center;
  margin: 0.5rem 0rem;
  background: var(--grey);
  border-radius: 10px;

  input {
    display: block;
    border: 0;
    background: transparent;
    font-size: 1rem;
    flex: 1;
    padding: 0.55rem;
    border: 2px solid transparent;
    border-radius: 10px;
    cursor: text;
    ::-webkit-calendar-picker-indicator {
      opacity: 0.2;
      cursor: pointer;
    }
  }

  svg {
    color: var(--pink);
    margin-left: 10px;
  }
`;
