import { ContainerDate, ContentInputDate } from "./style";

export const InputDate = ({ icon: Icon, label, ...rest }) => {
  return (
    <ContainerDate>
      <span> {label} </span>
      <ContentInputDate>
        {Icon && <Icon />}

        <input type="datetime-local" {...rest} />
      </ContentInputDate>
    </ContainerDate>
  );
};
