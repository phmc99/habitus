import { Input } from "../Input";
import { useAuth } from "../../providers/Auth";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useHistory } from "react-router-dom";
import { IoMdPerson, IoMdLock } from "react-icons/io";

import ButtonSubmit from "../ButtonSubmit";

const FormLogin = () => {
  const schema = yup.object().shape({
    username: yup.string().required("Campo obrigatório"),
    password: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const history = useHistory();
  const { signIn } = useAuth();

  const onSubmit = (user) => {
    signIn(user, history);
    reset();
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Input
        icon={IoMdPerson}
        label="Username"
        type="text"
        placeholder="Digite seu nome"
        {...register("username")}
      />
      <span className="span-error">{errors.username?.message}</span>

      <Input
        icon={IoMdLock}
        label="Password"
        type="password"
        placeholder="Digite sua senha"
        {...register("password")}
      />
      <span className="span-error">{errors.password?.message}</span>

      <ButtonSubmit type="submit">Entrar</ButtonSubmit>
    </form>
  );
};

export default FormLogin;
