import toast from "react-hot-toast";
import { useHistory } from "react-router-dom";
import { useUserGroupDetails } from "../../providers/UserGroupDetails";
import api from "../../services/api";
import ButtonAction from "../ButtonAction";
import ModalBase from "../ModalBase";
import { RoundedCard, GroupDetailContent, BoxInfo } from "./style";

const ModalGroupDetail = ({ item, setModal }) => {
  const history = useHistory();

  const { setGroupId } = useUserGroupDetails();

  const handleSubscribe = async () => {
    const token = await JSON.parse(localStorage.getItem("@habits:token"));
    await api
      .post(
        `/groups/${item.id}/subscribe/`,
        {},
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then(() => {
        toast.success(`Agora você faz parte do ${item.name}!`);
        setGroupId(item.id);
      });
    setModal(false);
    history.push(`group/${item.id}`);
  };

  return (
    <ModalBase closeModal={setModal}>
      <GroupDetailContent>
        <div className="header">
          <h2>{item.name}</h2>
          <span>{item.category}</span>
        </div>
        <div className="container-sum">
          <div className="container-rounded">
            <div className="rounded-unit">
              <span>Usuários</span>
              <RoundedCard>{item["users_on_group"].length}</RoundedCard>
            </div>
            <div className="rounded-unit">
              <span>Meta</span>
              <RoundedCard pink>{item.goals.length}</RoundedCard>
            </div>
            <div className="rounded-unit">
              <span>Atividade</span>
              <RoundedCard>{item.activities.length}</RoundedCard>
            </div>
          </div>
        </div>
        <div>
          <span>Descrição</span>
          <BoxInfo>{item.description}</BoxInfo>
        </div>
        <ButtonAction onClick={handleSubscribe}>Inscrever</ButtonAction>
      </GroupDetailContent>
    </ModalBase>
  );
};

export default ModalGroupDetail;
