import styled from "styled-components";

export const GroupDetailContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  min-width: 300px;
  width: 80vw;
  max-width: 420px;
  height: 45vh;
  justify-content: space-between;

  .header {
    text-align: center;
  }

  .container-rounded {
    display: flex;
    flex-wrap: wrap;
  }

  .rounded-unit {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 0.8rem;
  }

  .container-sum {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;

export const RoundedCard = styled.div`
  width: 45px;
  height: 45px;
  border-radius: 50%;
  background-color: ${(props) =>
    props.pink ? `var(--pink)` : `var(--purple)`};
  color: ${(props) => (props.pink ? `var(--black)` : `var(--white)`)};
  font-size: 1.3rem;

  display: flex;
  justify-content: center;
  align-items: center;
`;

export const BoxInfo = styled.div`
  min-width: 260px;
  max-width: 280px;
  height: 30px;

  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;

  border-radius: var(--border);
  background-color: ${(props) =>
    props.pink ? `var(--pink)` : `var(--purple)`};
  color: ${(props) => (props.pink ? `var(--black)` : `var(--white)`)};

  padding: 0.3rem;
`;
