import ButtonAction from "../ButtonAction";
import { Input } from "../Input";
import ModalBase from "../ModalBase";
import { NewGroupForm } from "./style";
import { IoMdQuote } from "react-icons/io";

import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../services/api";
import toast from "react-hot-toast";
import { InputDate } from "../InputDate";

const ModalUpdateActivity = ({ setModal }) => {
  const token = JSON.parse(localStorage.getItem("@habits:token"));
  const id = JSON.parse(localStorage.getItem("@habits:item-id"));

  const schemaActivities = yup.object().shape({
    title: yup.string(),
    realization_time: yup.string(),
  });

  const { register, handleSubmit } = useForm({
    resolver: yupResolver(schemaActivities),
  });

  const onSubmit = async (data) => {
    await api
      .patch(`/activities/${id}/`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => {
        toast.success("Atividade atualizada!");
      })
      .catch(() => toast.error("Ops, algo deu errado!"));

    setModal(false);
  };

  const deleteItem = async () => {
    await api
      .delete(`/activities/${id}/`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => {
        toast.success("A atividade foi excluida!");
      })
      .catch(() => toast.error("Ops, algo deu errado!"));

    setModal(false);
  };

  return (
    <ModalBase closeModal={setModal}>
      <NewGroupForm>
        <h2>Atualize a atividade</h2>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            label="Titulo"
            icon={IoMdQuote}
            placeholder="Novo titulo da atividade"
            {...register("title")}
          />

          <InputDate
            label={"Prazo"}
            placeholder={`Nova prazo da atividade`}
            {...register("realization_time")}
          />

          <ButtonAction type="submit">Atualizar</ButtonAction>
        </form>
        <ButtonAction onClick={deleteItem}>Deletar</ButtonAction>
      </NewGroupForm>
    </ModalBase>
  );
};

export default ModalUpdateActivity;
