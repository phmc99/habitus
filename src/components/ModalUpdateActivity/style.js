import styled from "styled-components";

export const NewGroupForm = styled.div`
  width: 70vw;
  max-width: 450px;

  form {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  select {
    padding: 0.7rem;
    border-radius: 10px;
    border: 0;
    width: 100%;
  }

  .container-input {
    background: var(--purple);

    svg {
      color: var(--white);
    }
  }

  input,
  select {
    background: var(--purple);
    color: var(--white);
    font-size: 1rem;
  }
`;
