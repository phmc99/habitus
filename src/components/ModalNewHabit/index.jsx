import React from "react";
import ButtonAction from "../ButtonAction";
import { Input } from "../Input";
import ModalBase from "../ModalBase";
import { NewHabitForm } from "./style";
import {
  IoIosStats,
  IoIosTimer,
  IoMdPricetag,
  IoMdQuote,
} from "react-icons/io";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useHabitsCreate } from "../../providers/HabitsCreate";

const ModalNewHabit = ({ setModal }) => {
  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    category: yup.string().required("Campo obrigatório"),
    difficulty: yup.string().required("Campo obrigatório"),
    frequency: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const { postHabits } = useHabitsCreate();

  const id = JSON.parse(localStorage.getItem("@habits:id"));

  const onSubmit = (data) => {
    const body = {
      ...data,
      achieved: false,
      how_much_achieved: 0,
      user: id,
    };
    postHabits(body);
    setModal(false);
  };

  return (
    <ModalBase closeModal={setModal}>
      <NewHabitForm>
        <h2>Novo hábito</h2>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            label="Nome"
            icon={IoMdQuote}
            type="text"
            placeholder="Nome do hábito"
            {...register("title")}
          />
          <span className="span-error">{errors.title?.message}</span>

          <Input
            label="Categoria"
            icon={IoMdPricetag}
            type="text"
            placeholder="Categoria do hábito"
            {...register("category")}
          />
          <span className="span-error">{errors.category?.message}</span>

          <Input
            label="Dificuldade"
            icon={IoIosStats}
            type="text"
            placeholder="Grau de dificuldade do hábito"
            {...register("difficulty")}
          />
          <span className="span-error">{errors.difficulty?.message}</span>

          <Input
            label="Frequência"
            icon={IoIosTimer}
            type="text"
            placeholder="Frequência do hábito"
            {...register("frequency")}
          />
          <span className="span-error">{errors.frequency?.message}</span>

          <ButtonAction type="submit">Criar</ButtonAction>
        </form>
      </NewHabitForm>
    </ModalBase>
  );
};

export default ModalNewHabit;
