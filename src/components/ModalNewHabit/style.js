import styled from "styled-components";

export const NewHabitForm = styled.div`
  width: 70vw;
  max-width: 450px;

  form {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;
