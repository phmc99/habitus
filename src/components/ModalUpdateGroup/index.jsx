import ButtonAction from "../ButtonAction";
import { Input } from "../Input";
import ModalBase from "../ModalBase";
import { NewGroupForm } from "./style";
import { IoMdPaper, IoMdPricetag, IoMdQuote } from "react-icons/io";

import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../services/api";
import toast from "react-hot-toast";

const ModalUpdateGroup = ({ setModal }) => {
  const schema = yup.object().shape({
    name: yup.string(),
    description: yup.string(),
    category: yup.string(),
  });

  const { register, handleSubmit } = useForm({ resolver: yupResolver(schema) });

  const onSubmit = (data) => {
    const token = JSON.parse(localStorage.getItem("@habits:token"));
    const id = JSON.parse(localStorage.getItem("@habits:group-id"));

    api
      .patch(`/groups/${id}/`, data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(() => {
        toast.success("Grupo atualizado!");
      })
      .catch(() => toast.error("Ops, algo deu errado!"));

    setModal(false);
  };

  return (
    <ModalBase closeModal={setModal}>
      <NewGroupForm>
        <h2>Atualize o seu Grupo</h2>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            label="Nome"
            icon={IoMdQuote}
            placeholder="Novo nome do grupo"
            {...register("name")}
          />

          <Input
            label="Descrição"
            icon={IoMdPaper}
            placeholder="Nova descrição do grupo"
            {...register("description")}
          />
          <Input
            label="Categoria"
            icon={IoMdPricetag}
            placeholder="Nova categoria do grupo"
            {...register("category")}
          />

          <ButtonAction type="submit">Atualizar</ButtonAction>
        </form>
      </NewGroupForm>
    </ModalBase>
  );
};

export default ModalUpdateGroup;
