import { Container, ContentInput } from "./styled";

export const Input = ({ icon: Icon, label, ...rest }) => {
  return (
    <Container>
      <span> {label} </span>
      <ContentInput className="container-input">
        {Icon && <Icon />}
        <input autoComplete={false} {...rest} />
      </ContentInput>
    </Container>
  );
};
