import React from "react";
import ButtonAction from "../ButtonAction";
import { Input } from "../Input";
import ModalBase from "../ModalBase";
import { NewGoalForm } from "./style";
import { IoIosStats, IoMdQuote } from "react-icons/io";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useGoalCreate } from "../../providers/GoalCreate";
import { useParams } from "react-router-dom";

const ModalNewGoal = ({ setModal }) => {
  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    difficulty: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const { id } = useParams();
  const { postGoals } = useGoalCreate();

  const onSubmit = (data) => {
    const body = {
      ...data,
      group: id,
      how_much_achieved: 0,
    };

    postGoals(body);
    setModal(false);
  };

  return (
    <ModalBase closeModal={setModal}>
      <NewGoalForm>
        <h2>Nova meta</h2>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            label="Nome da nova meta"
            icon={IoMdQuote}
            type="text"
            placeholder="Nome da nova atividade"
            {...register("title")}
          />
          <span className="span-error">{errors.title?.message}</span>

          <Input
            label="Dificuldade"
            icon={IoIosStats}
            type="text"
            placeholder="Nome da nova atividade"
            {...register("difficulty")}
          />
          <span className="span-error">{errors.difficulty?.message}</span>

          <ButtonAction type="submit">Criar</ButtonAction>
        </form>
      </NewGoalForm>
    </ModalBase>
  );
};

export default ModalNewGoal;
