import styled from "styled-components";

export const GroupSearchBox = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 50px;

  .form {
    display: flex;
    flex-direction: column;
    text-align: center;

    .button-container {
      margin-top: 10px;
      display: flex;
      gap: 10px;
    }
  }

  div {
    margin: 0;
  }
  @media (min-width: 768px) {
    .form {
      gap: 20px;
      display: flex;
      flex-direction: row;
      text-align: center;
      align-items: baseline;
    }

    .list {
      max-width: 1200px;

      li {
        width: 300px;
      }
    }
  }
`;
