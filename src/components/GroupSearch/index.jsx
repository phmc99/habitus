import { useState } from "react";
import { useGroups } from "../../providers/GroupsGet";
import { Input } from "../Input";
import ButtonSubmit from "../ButtonSubmit";
import Card from "../Card";
import { IoIosSearch } from "react-icons/io";
import { GroupSearchBox } from "./style";
const GroupSearch = ({ handleClick }) => {
  const [groupSearch, setGroupSearch] = useState("");
  const [filteredGroup, setFilteredGroup] = useState([]);

  const { allGroups } = useGroups();

  const showGroups = (groupSearch) => {
    if (groupSearch === "") {
      setFilteredGroup([]);
    } else {
      const searched = allGroups.filter((group) => {
        return group.name
          .toLowerCase()
          .includes(groupSearch.toLowerCase().trim());
      });
      setFilteredGroup(searched);
    }
  };

  const clearSearch = () => {
    showGroups("");
    setGroupSearch("");
  };

  return (
    <GroupSearchBox>
      <div className="form">
        <Input
          type="text"
          value={groupSearch}
          onChange={(e) => setGroupSearch(e.target.value)}
          icon={IoIosSearch}
          placeholder="Busque um grupo"
        />
        <div className="button-container">
          <ButtonSubmit type="submit" onClick={() => showGroups(groupSearch)}>
            Pesquisar
          </ButtonSubmit>
          <ButtonSubmit type="submit" onClick={() => clearSearch()}>
            Limpar
          </ButtonSubmit>
        </div>
      </div>
      <ul className="list">
        {filteredGroup &&
          filteredGroup.map((group, index) => {
            return (
              <li key={index}>
                <Card
                  name={group.name}
                  category={group.category}
                  onClick={() => handleClick(group)}
                />
              </li>
            );
          })}
      </ul>
    </GroupSearchBox>
  );
};

export default GroupSearch;
