import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useHistory } from "react-router-dom";
import toast, { Toaster } from "react-hot-toast";
import api from "../../services/api";
import ButtonSubmit from "../ButtonSubmit";
import { FormStyle, InputContainer } from "./styles";
import { Input } from "../Input";
import { IoMdPerson, IoMdLock, IoIosMail } from "react-icons/io";

const RegisterForm = () => {
  const formSchema = yup.object().shape({
    username: yup.string().required("Campo obrigatório"),
    email: yup.string().required("E-mail obrigatório").email("E-mail inválido"),
    password: yup.string().required("Senha obrigatória"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const history = useHistory();

  const onSubmitFunction = (data) => {
    api
      .post("/users/", data)
      .then(() => {
        reset();
        toast.success("Conta criada com sucesso")
        history.push("/login");
      })
      .catch((e) => {
        if (e) {
          toast.error(`Erro ao cadastrar usuário`);
        }
      });
  };

  return (
    <div>
      <FormStyle onSubmit={handleSubmit(onSubmitFunction)}>
        <InputContainer>
          <Input
            label="Nome de Usuário"
            id="userName"
            placeholder="Digite seu nome de usuário"
            {...register("username")}
            helperText={errors.username?.message}
            icon={IoMdPerson}
          />
          <span className="span-error">{errors.username?.message}</span>
        </InputContainer>
        <InputContainer>
          <Input
            label="E-mail"
            id="email"
            placeholder="Digite seu email"
            {...register("email")}
            helperText={errors.email?.message}
            icon={IoIosMail}
          />
          <span className="span-error">{errors.email?.message}</span>
        </InputContainer>
        <InputContainer>
          <Input
            label="Senha"
            id="password"
            type="password"
            placeholder="Digite sua senha"
            {...register("password")}
            helperText={errors.password?.message}
            icon={IoMdLock}
          />
          <span className="span-error">{errors.password?.message}</span>
        </InputContainer>

        <ButtonSubmit type="submit">Enviar</ButtonSubmit>
      </FormStyle>

      <div>
        <Toaster />
      </div>
    </div>
  );
};

export default RegisterForm;
