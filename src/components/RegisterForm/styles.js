import styled from "styled-components";

export const FormStyle = styled.form`
  width: 90vw;
  display: flex;
  flex-direction: column;
  margin-bottom: 80px;

  @media (min-width: 768px) {
    width: 45vw;
    max-width: 450px;
  }
`;

export const InputContainer = styled.div`
  width: 100%;
`;
