import styled from "styled-components";

export const ButtonBackground = styled.div`
  width: 50px;
  height: 50px;
  border-radius: 50%;
  background-color: var(--pink);
  color: var(--black);

  display: flex;
  justify-content: center;
  align-items: center;

  position: fixed;
  bottom: 20px;
  right: 20px;

  cursor: pointer;

  svg {
    font-size: 2.4rem;
  }
`;
