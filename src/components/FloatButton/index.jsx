import { ButtonBackground } from "./style";
import { IoIosAdd } from "react-icons/io";

const FloatButton = ({ ...rest }) => {
  return (
    <ButtonBackground {...rest}>
      <IoIosAdd />
    </ButtonBackground>
  );
};

export default FloatButton;
