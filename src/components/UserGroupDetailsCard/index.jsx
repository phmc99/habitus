import { IoMdCreate } from "react-icons/io";
import { CardBox } from "./styles";

const UserGroupDetailsCard = ({ item, type, setUpdateModal }) => {
  const handleClick = () => {
    setUpdateModal(true);
    JSON.stringify(localStorage.setItem("@habits:item-id", item.id));
  };

  return (
    <CardBox className={`${type} ${item.achieved && "achieved"}`}>
      <div className="info">
        <div className="title">
          <strong>{item.title}</strong>
        </div>
        <span>
          {type === "goals"
            ? `${item.difficulty} | Progresso: ${item.how_much_achieved}%`
            : `Data: ${item["realization_time"].substr(0, 10)}`}
        </span>
      </div>
      <span className="edit-activity" onClick={handleClick}>
        <IoMdCreate />
      </span>
    </CardBox>
  );
};

export default UserGroupDetailsCard;
