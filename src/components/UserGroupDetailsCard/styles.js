import styled from "styled-components";

export const CardBox = styled.li`
  width: 80vw;
  height: 100px;
  background-color: var(--pink);
  border-radius: 10px;
  padding: 5px;

  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 15px;

  strong {
    font-size: 1.1rem;
  }

  span {
    font-size: 1.1rem;
  }

  @media (min-width: 900px) {
    width: 40vw;
  }

  .info {
    display: flex;
    flex-direction: column;

    .title {
      width: 100px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;

      @media (min-width: 768px) {
        width: 300px;
      }
    }
  }

  .edit-activity {
    cursor: pointer;
    width: 50px;
    height: 50px;

    display: flex;
    justify-content: center;
    align-items: center;
    svg {
      font-size: 1.4rem;
    }
  }
`;
