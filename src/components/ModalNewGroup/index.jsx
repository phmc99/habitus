import ButtonAction from "../ButtonAction";
import { Input } from "../Input";
import ModalBase from "../ModalBase";
import { NewGroupForm } from "./style";
import { IoMdPaper, IoMdPricetag, IoMdQuote } from "react-icons/io";

import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../services/api";
import toast from "react-hot-toast";
import { useHistory } from "react-router-dom";
import { useAuth } from "../../providers/Auth";
import { useUserGroupDetails } from "../../providers/UserGroupDetails";

const ModalNewGroup = ({ setModal }) => {
  const history = useHistory();

  const schema = yup.object().shape({
    name: yup.string().required("Campo obrigatório"),
    description: yup.string().required("Campo obrigatório"),
    category: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const { token } = useAuth();
  const { setGroupId } = useUserGroupDetails();
  const onSubmit = (data) => {
    api
      .post("/groups/", data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        toast.success("Grupo criado!");
        setGroupId(response.data.id);
        history.push(`/group/${response.data.id}`);
      })
      .catch(() => toast.error("Ops, algo deu errado!"));

    setModal(false);
  };

  return (
    <ModalBase closeModal={setModal}>
      <NewGroupForm>
        <h2>Novo Grupo</h2>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            label="Nome"
            icon={IoMdQuote}
            placeholder="Nome do grupo"
            {...register("name")}
          />
          <span className="span-error">{errors.name?.message}</span>

          <Input
            label="Descrição"
            icon={IoMdPaper}
            placeholder="Descrição do grupo"
            {...register("description")}
          />
          <span className="span-error">{errors.description?.message}</span>
          <Input
            label="Categoria"
            icon={IoMdPricetag}
            placeholder="Categoria do grupo"
            {...register("category")}
          />
          <span className="span-error">{errors.category?.message}</span>

          <ButtonAction type="submit">Criar</ButtonAction>
        </form>
      </NewGroupForm>
    </ModalBase>
  );
};

export default ModalNewGroup;
