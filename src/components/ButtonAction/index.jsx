import React from "react";
import { ActionsButton } from "./style";

const ButtonAction = ({ ...rest }) => {
  return <ActionsButton {...rest} />;
};

export default ButtonAction;
