import styled from "styled-components";

export const ActionsButton = styled.button`
  border: 0;
  width: 120px;
  padding: 8px 20px;
  border-radius: 5px;
  background-color: ${(props) =>
    props.purple ? `var(--purple)` : `var(--pink)`};
  /* color: ${(props) => (props.purple ? `var(--white)` : `var(--black)`)}; */
  color: var(--white);
  font-weight: bolder;
  font-size: 1rem;
  margin-top: 20px;
  cursor: pointer;
  width: 100%;
`;
