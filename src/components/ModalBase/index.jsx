import React from "react";
import { CloseButton, ModalBackground, ModalBox } from "./style";
import { IoIosClose } from "react-icons/io";

const ModalBase = ({ closeModal, children }) => {
  return (
    <ModalBackground>
      <ModalBox>
        <CloseButton onClick={() => closeModal(false)}>
          <IoIosClose />
        </CloseButton>
        {children}
      </ModalBox>
    </ModalBackground>
  );
};

export default ModalBase;
