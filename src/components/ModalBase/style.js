import styled, { keyframes } from "styled-components";

const FadeInAnimation = keyframes`
from {
    opacity: 0;
}
to {
    opacity: 1;
}
`;

export const ModalBackground = styled.div`
  width: 100%;
  height: 100%;

  background-color: rgb(0, 0, 0, 0.75);

  position: fixed;

  z-index: 2;
`;

export const ModalBox = styled.div`
  animation: ${FadeInAnimation} 2s;
  max-width: 90vw;
  max-height: 80vh;

  padding: 20px;
  border-radius: 10px;
  background-color: var(--violet);

  display: flex;
  flex-direction: column;
  align-items: center;

  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const CloseButton = styled.button`
  border: 0;
  width: 25px;
  height: 25px;
  border-radius: 5px;
  background-color: var(--pink);
  cursor: pointer;

  text-align: center;
  font-size: 24px;

  position: absolute;
  right: 20px;
  top: 10px;

  svg {
    color: var(--white);
  }
`;
