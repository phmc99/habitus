import styled from "styled-components";

export const Container = styled.div`
  min-width: 300px;
  width: 85vw;
  max-width: 400px;
  height: 80px;
  padding: 20px;
  margin: 0 10px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  justify-content: flex-start;

  .UserImage {
    width: 25%;

    img {
      width: 65px;
      border: 2px ridge var(--pink);
      border-radius: 100%;
    }
  }

  p {
    font-size: 1.2rem;
  }
  span {
    font-size: 1.1.rem;
  }
`;
