import { Container } from "./style";
// import logo from "../../assets/logo.png";

const UsersCard = ({ item }) => {
  return (
    <Container>
      <div className="UserImage">
        <img src={`https://picsum.photos/id/${item.id}/200/200`} alt="logo" />
      </div>
      <div className="UserData">
        <p>{item.username}</p>
        <span>{item.email}</span>
      </div>
    </Container>
  );
};

export default UsersCard;
