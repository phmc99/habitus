import api from "../../services/api";
import ButtonAction from "../ButtonAction";
import ModalBase from "../ModalBase";
import { UserProfileContainer} from "./styled";
import toast from "react-hot-toast";
import { useState } from "react";
import { Input } from "../Input";
import { IoMdPerson, IoMdLock, IoIosMail } from "react-icons/io";
import { useEffect } from "react";

const ModalUserProfile = ({ item, setModal }) => {
  const [userDetail, setUserDetail] = useState([]);
  const [newUserName, setNewUserName] = useState('');
  const [newEmail, setNewEmail] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [visibleEditUser, setVisibleEditUser] = useState(false);
  const [visibleEditEmail, setVisibleEditEmail] = useState(false);
  const [visibleEditPassword, setVisibleEditPassword] = useState(false);
  const handleEditUserName = () => {
    setVisibleEditUser(!visibleEditUser);
  }
  const handleEditEmail = () => {
    setVisibleEditEmail(!visibleEditEmail);
  }
  const handleEditPassword = () => {
    setVisibleEditPassword(!visibleEditPassword);
  }
  const handleInputUserName = e => setNewUserName(e.target.value);
  const handleInputEmail = e => setNewEmail(e.target.value);
  const handleInputPassword = e => setNewPassword(e.target.value);
  const handleChange = async (key, value) => {
    const token = await JSON.parse(localStorage.getItem("@habits:token"));
    const id = await JSON.parse(localStorage.getItem("@habits:id"));
    
    await api
      .patch(
        `/users/${id}/`,
        {
          [key]: `${value}`
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then(() => {
        toast.success(`${key} editado com sucesso!`);
      });
    setNewUserName('');
    setNewEmail('');
    
    setVisibleEditUser(false);
    setVisibleEditEmail(false);
    
  }
  const getUser = async () => {
    const token = await JSON.parse(localStorage.getItem("@habits:token"));
    const id = await JSON.parse(localStorage.getItem("@habits:id"));
    await api
      .get(
        `/users/${id}/`,
        {},
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        setUserDetail(response.data);
      });
  }
  
  useEffect(() => {
    getUser();
  }, [newUserName, newEmail, newPassword]);
  
  return (<>
    <ModalBase closeModal={setModal}>
      <UserProfileContainer>
        <h2>Sobre mim</h2>

        <div className="line-info">
          <span>
            Nome de usuário:&nbsp;
            <span className="light">{userDetail.username}</span>
          </span>
          <ButtonAction
            onClick={handleEditUserName}
            purple>
            Editar
          </ButtonAction>
        </div>
        <div className={!visibleEditUser ? `hidden` : `line-edit`}>
          <div className="line-info">
            <Input
              id="userName"
              placeholder="Novo nome de usuário"
              icon={IoMdPerson}
              onChange={handleInputUserName}
              value= {newUserName}
              />
            <ButtonAction
              onClick={() => handleChange("username", newUserName)}>
              Salvar
            </ButtonAction>
          </div>
        </div>

        <div
          className="line-info">
          <span>
            E-mail:&nbsp;
            <span className="light">{userDetail.email}</span>
          </span>
          <ButtonAction
            onClick={handleEditEmail}
            purple>
            Editar
          </ButtonAction>
        </div>
        <div className={!visibleEditEmail ? `hidden` : `line-edit`}>
          <div
            className="line-info">
            <Input
              id="Email"
              placeholder="Novo e-mail"
              icon={IoIosMail}
              onChange={handleInputEmail}
              value= {newEmail}
            />
            <ButtonAction
              onClick={() => handleChange("email", newEmail)}>
              Salvar
            </ButtonAction>
          </div>
        </div>

        <div
          className="line-info">
          <span>
            Senha:&nbsp;
            <span className="light">******</span>
          </span>
          <ButtonAction
            onClick={handleEditPassword}
            purple>
            Editar
          </ButtonAction>
        </div>
        <div className={!visibleEditPassword ? `hidden` : `line-edit`}>
          <div
            className="line-info">
            <Input
              id="Password"
              placeholder="Nova senha"
              icon={IoMdLock}
              onChange={handleInputPassword}
              value= {newPassword}
            />
            <ButtonAction onClick={() => {
              toast('Funcionalidade indisponível na API.', { icon: '🔧', });
              setNewPassword('');
              setVisibleEditPassword(false);
            }}>
              Salvar
            </ButtonAction>
          </div>
        </div>
      </UserProfileContainer>
    </ModalBase>
  </>)
}

export default ModalUserProfile;