import styled from "styled-components";

export const UserProfileContainer = styled.div`
  min-width: 300px;
  width: 80vw;
  max-width: 620px;

  max-height: 80vh;

  display: flex;
  flex-direction: column;
  align-items: center;

  h2 {
    margin-bottom: 30px;
  }
  @media (min-width: 768px) {
    /* width: 100vw; */
  }

  input {
    width: 150px;

    @media (min-width: 768px) {
    }
  }

  .container-input {
    margin: 0;
    width: 94%;
    height: 50%;

    @media (min-width: 768px) {
      width: 70%;
    }
  }

  button {
    margin: 0;
    width: 65px;
    height: 35px;

    display: flex;
    align-items: center;
    justify-content: center;
  }

  span {
    font-weight: bolder;
    margin-top: -0.1rem;
  }

  .light {
    font-weight: lighter;
  }

  .line-info {
    margin-top: 10px;
    padding: 0;

    width: 80%;

    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .hidden {
    visibility: hidden;
  }

  .line-edit {
    width: 80%;
    display: flex;
    justify-content: flex-start;
    align-items: center;

    @media (min-width: 768px) {
      width: 100%;
      justify-content: center;
    }
  }
`;
