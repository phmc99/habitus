import { useUsersGet } from "../../providers/UsersGet";
import UsersCard from "../UsersCard";
import { Container, Button } from "./style";
import { FiChevronsLeft, FiChevronsRight } from "react-icons/fi";

const UsersList = () => {
  const { users, previousPage, nextPage, next, last } = useUsersGet();
  // console.log(users);
  return (
    <Container>
      <ul>
        {users.map((data) => (
          <li key={data.id}>
            <UsersCard item={data} />
          </li>
        ))}
      </ul>
      <div className="Buttons">
        <Button disabled={next === 1} onClick={previousPage}>
          <FiChevronsLeft />
        </Button>
        <span>{next}</span>
        <Button disabled={last === null} onClick={nextPage}>
          <FiChevronsRight />
        </Button>
      </div>
    </Container>
  );
};

export default UsersList;
