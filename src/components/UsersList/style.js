import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 90vw;
  margin: 0 auto;

  ul {
    margin-top: 30px;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly;

    li:nth-child(odd) {
      background-color: var(--violet);
    }

    li:nth-child(even) {
      background-color: var(--purple);
      color: var(--white);
    }

    @media (min-width: 978px) {
      margin-top: 50px;
    }
  }

  li {
    list-style: none;
    margin: 10px;
    border-radius: 10px;
    box-shadow: 6px 6px 12px 3px var(--darkGrey);
  }

  .Buttons {
    display: flex;
    align-items: center;
    margin-top: 20px;

    span {
      font-size: 1.5rem;
      color: var(--purple);
    }
  }
`;

export const Button = styled.button`
  width: 50px;
  height: 50px;
  background-color: transparent;
  border: 1px solid transparent;
  cursor: pointer;

  svg {
    font-size: 3rem;
    color: var(--pink);
  }

  :disabled {
    svg {
      color: var(--violet);
      cursor: not-allowed;
    }
  }
`;
