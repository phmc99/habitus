import React from "react";
import ButtonAction from "../ButtonAction";
import { Input } from "../Input";
import { InputDate } from "../InputDate";
import ModalBase from "../ModalBase";
import { NewActivityForm } from "./style";
import { IoIosCalendar, IoMdQuote } from "react-icons/io";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useActivityCreate } from "../../providers/ActivityCreate";
import { useParams } from "react-router-dom";

const ModalNewActivity = ({ setModal }) => {
  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    realization_time: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const { id } = useParams();

  const { postActivity } = useActivityCreate();

  const onSubmit = (data) => {
    const body = {
      ...data,
      group: id,
    };
    postActivity(body);
    setModal(false);
  };

  return (
    <ModalBase closeModal={setModal}>
      <NewActivityForm>
        <h2>Nova atividade</h2>

        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            label="Título"
            icon={IoMdQuote}
            type="text"
            placeholder="Nome da nova atividade"
            {...register("title")}
          />
          <span className="span-error">{errors.title?.message}</span>

          <InputDate
            label="Acontece em"
            icon={IoIosCalendar}
            {...register("realization_time")}
          />
          <span className="span-error">{errors.realization_time?.message}</span>

          <ButtonAction type="submit">Adicionar</ButtonAction>
        </form>
      </NewActivityForm>
    </ModalBase>
  );
};

export default ModalNewActivity;
