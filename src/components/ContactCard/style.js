import styled from "styled-components";

export const ContactCardBox = styled.div`
  width: 100%;
  height: 190px;
  background-color: var(--violet);
  border-radius: 10px;
  padding: 20px;
  margin: 0 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  box-shadow: 6px 6px 12px 3px var(--darkGrey);

  h2 {
    font-size: 28px;
  }

  span {
    font-size: 22px;
  }

  .contact-links {
    a {
      color: var(--black);
      svg {
        font-size: 32px;
      }
    }
  }
`;
