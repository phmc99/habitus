import { RiGitlabFill } from "react-icons/ri";
import { IoLogoLinkedin } from "react-icons/io";
import { ContactCardBox } from "./style";

const ContactCard = ({ devName, cargo, gitlab, linkedin }) => {
  return (
    <ContactCardBox>
      <h2>{devName}</h2>
      <span>{cargo} </span>
      <div className="contact-links">
        <a
          href={`https://gitlab.com/${gitlab}`}
          target="_blank"
          rel="noreferrer"
        >
          <RiGitlabFill class="gitlab-icon" />
        </a>
        <a
          href={`https://www.linkedin.com/in/${linkedin}`}
          target="_blank"
          rel="noreferrer"
        >
          <IoLogoLinkedin class="linkedin-icon" />
        </a>
      </div>
    </ContactCardBox>
  );
};

export default ContactCard;
