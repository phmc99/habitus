import { AiOutlinePercentage } from "react-icons/ai";
import { IoIosClose } from "react-icons/io";
import { Input } from "../Input";
import { useForm } from "react-hook-form";
import { useAuth } from "../../providers/Auth";
import { Container } from "./styled";
import toast from "react-hot-toast";
import ButtonSubmit from "../../components/ButtonSubmit";
import api from "../../services/api";
import { CloseButton } from "../ModalBase/style";

const FormMyHabit = ({ habit, ...rest }) => {
  const { register, handleSubmit } = useForm();
  const { token } = useAuth();
  const { setClose, setModal } = rest;

  const onClick = async (update) => {
    const { id } = habit;

    await api.patch(`/habits/${id}/`, update, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    toast.success("Atualizado com sucesso");
    setClose(false);
    setModal(false);
  };

  return (
    <Container>
      <CloseButton onClick={() => setClose(false)}>
        <IoIosClose />
      </CloseButton>
      <div className="content">
        <h2>Meu Hábito</h2>
        <form onSubmit={handleSubmit(onClick)}>
          <Input
            label="Quanto que já progrediu?"
            placeholder="Progresso"
            type="number"
            min="0"
            max="100"
            icon={AiOutlinePercentage}
            {...register("how_much_achieved")}
          />

          <label>Atingiu seu objetivo?</label>
          <select {...register("achieved")}>
            <option value="true">Sim</option>
            <option selected value="false">
              Não
            </option>
          </select>

          <div className="div-button">
            <ButtonSubmit type="submit">Salvar</ButtonSubmit>
          </div>
        </form>
      </div>
    </Container>
  );
};
export default FormMyHabit;
