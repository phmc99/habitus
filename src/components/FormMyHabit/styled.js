import styled from "styled-components";
import { keyframes } from "styled-components";

const InputAnimation = keyframes`
     0% { 
        position: relative;
        transform: translateX(-300px);
    }
    
    100% { transform: translateX(0px);}
`;

const ButtonAnimaion = keyframes`
    0%{
        position: relative;
        transform: translateY(100px);
    }
    100%{}
`;

export const Container = styled.div`
  position: absolute;
  min-width: 300px;
  width: 50vw;
  max-width: 430px;
  top: 0px;
  padding: 1rem;
  background: var(--violet);
  border-radius: 15px;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 410px;

  .content {
    width: 100%;
  }

  svg {
    color: var(--white);
  }

  .span-icon {
    position: absolute;
    left: 255px;
    top: 8px;
    display: flex;
    align-items: center;
    justify-content: center;
    background: var(--pink);
    border-radius: 50%;
    width: 20px;
    height: 20px;
  }
  .span-icon svg {
    color: var(--white);
  }

  .container-input {
    background: var(--purple);
    animation: ${InputAnimation} 2s normal;
  }

  h2 {
    margin-bottom: 1rem;
  }

  input label {
    position: absolute;
    font-weight: bold;
  }

  select {
    animation: ${InputAnimation} 2s normal;
  }
  form {
    overflow: hidden;
    display: flex;
    flex-direction: column;
    padding: 0.5rem;
    height: 90%;
  }
  select {
    padding: 0.7rem;
    border-radius: 10px;
    border: 0;
    width: 100%;
  }

  input,
  select {
    background: var(--purple);
    color: var(--white);
  }

  option {
    color: var(--black);
    border-radius: 100px;
    background: var(--violet);
  }
  .div-button {
    width: 100%;
    margin-top: 1rem;
    text-align: center;
  }
  .div-button button {
    background: var(--pink);
    color: var(--white);
    animation: ${ButtonAnimaion} 1.5s normal;
  }
`;
