import styled, { keyframes } from "styled-components";

const appearFromLeft = keyframes`
from {
    opacity: 0;
    transform: translateX(-50px)
}
to {
    opacity: 1;
    transform: translateX(0px);
}
`;

export const Background = styled.div`
  width: 100%;
  height: 100%;
  background-color: rgb(0, 0, 0, 0.55);
  position: fixed;
  z-index: 2;
  top: 0;
  right: 0;
`;

export const Menu = styled.div`
  background-color: var(--purple);
  color: var(--white);
  display: flex;
  flex-direction: column;
  width: 66%;
  max-width: 300px;
  height: 100vh;
  padding: 10px;
  text-align: left;
  position: fixed;
  animation: ${appearFromLeft} 1s;
  z-index: 3;
  overflow-y: scroll;
  justify-content: space-between;
  top: 0;

  ::-webkit-scrollbar {
    width: 8px;
    height: 8px;
  }
  ::-webkit-scrollbar-thumb {
    background: var(--pink);
    border-radius: 15px;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #b3486f;
    cursor: pointer;
  }
  ::-webkit-scrollbar-track {
    background: #f0f0f0;
    border-radius: 0px;
    box-shadow: inset 0px 0px 0px 0px #f0f0f0;
  }

  .separator {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }

  h2 {
    text-align: center;
    margin-bottom: 4vh;
  }

  ul {
    padding: 0;
  }

  ul > li {
    list-style: none;
    margin-bottom: 3vh;
  }
  details > summary {
    cursor: pointer;
    list-style: none;
  }

  details > summary::after {
    content: " ▼ ";
  }

  details[open] > summary::after {
    content: " ▲ ";
  }
  details {
    margin-bottom: 2vh;
    text-align: left;
    display: flex;
    flex-direction: column;

    div {
      margin-left: 25px;
    }
  }
  button {
    bottom: 1vw;
    left: 10px;
    border: none;
    background-color: var(--purple);
    color: var(--white);
    text-align: left;
    cursor: pointer;
    font-size: 1.3rem;
    font-weight: bolder;
  }
  hr {
    margin-bottom: 3vh;
  }

  .menu-icon {
    position: absolute;
    top: 1%;
    left: 2%;
    font-size: 2rem;
  }

  .menu-footer {
    display: flex;
    flex-direction: column;
    gap: 10px;
  }

  @media (min-width: 768px) {
    .menu-icon {
      top: 2%;
      left: 1%;
      svg {
        cursor: pointer;
      }
    }
  }
`;

export const Span = styled.span`
  margin: 10px 0;
  cursor: pointer;
  font-size: 1.3rem;

  a {
    text-decoration: none;
    color: var(--white);
  }
`;

export const MenuOpenButton = styled.span`
  position: fixed;
  top: 5px;
  left: 5px;
  cursor: pointer;
  font-size: 2rem;
`;

export const MenuDesktop = styled.div`
  display: none;
  @media (min-width: 768px) {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    width: 100%;
    height: 100%;

    ul {
      margin-top: 70px;
      display: flex;
      flex-direction: column;
    }

    li {
      margin: 15px auto;
      list-style: none;
    }

    svg {
      font-size: 1.5rem;
    }

    .solid {
      margin: 15px 0;
    }

    .menu-footer {
      display: flex;
      flex-direction: column;

      span {
        margin: 10px auto;
      }
    }
  }
`;
