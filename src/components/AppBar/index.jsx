import { Menu, MenuOpenButton, Background, MenuDesktop } from "./styles";
import { Span } from "./styles";
import {
  IoIosBody,
  IoIosContacts,
  IoIosLogOut,
  IoIosMenu,
  IoIosPeople,
  IoIosSearch,
  IoMdCloseCircleOutline,
  IoMdPeople,
  IoMdPerson,
} from "react-icons/io";

import { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import api from "../../services/api";
import { useAuth } from "../../providers/Auth";
import { useUserGroupDetails } from "../../providers/UserGroupDetails";

const AppBar = () => {
  const [toggle, setToggle] = useState(false);
  const [subs, setSubs] = useState([]);
  const history = useHistory();
  const { setAuthenticated } = useAuth();

  const { setGroupId } = useUserGroupDetails();

  const token = JSON.parse(localStorage.getItem("@habits:token"));

  useEffect(() => {
    api
      .get("/groups/subscriptions/", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((resp) => setSubs(resp.data));
  }, [token]);

  const logout = () => {
    setAuthenticated(undefined);
    localStorage.clear();
    history.push("/");
  };

  const goToGroup = (id) => {
    setGroupId(id);
    history.push(`/group/${id}`);
  };

  return (
    <>
      {toggle ? (
        <>
          <Background onMouseEnter={() => setToggle(false)} />
          <Menu>
            <div className="separator">
              <span className="menu-icon" onClick={() => setToggle(false)}>
                <IoMdCloseCircleOutline />
              </span>
              <h2>Menu</h2>
              <ul className="dashboard-menu-itens">
                <li>
                  <Span>
                    <Link to="/about">
                      <IoMdPerson />
                      &nbsp; Sobre mim
                    </Link>
                  </Span>
                </li>
                <li>
                  <Span>
                    <Link to="/dashboard">
                      <IoIosBody />
                      &nbsp; Meus Hábitos
                    </Link>
                  </Span>
                </li>

                <li>
                  <details>
                    <summary>
                      <Span>
                        <IoIosPeople />
                        &nbsp; Meus grupos
                      </Span>
                    </summary>
                    {subs.map((item, index) => (
                      <Span key={index} onClick={() => goToGroup(item.id)}>
                        &nbsp;
                        {item.name}
                      </Span>
                    ))}
                  </details>
                </li>

                <hr className="solid" />
                <li>
                  <Span>
                    <Link to="/users">
                      <IoMdPeople />
                      &nbsp; Demais usuários
                    </Link>
                  </Span>
                </li>
                <li>
                  <Span>
                    <Link to="/groups">
                      <IoIosSearch />
                      &nbsp; Demais grupos
                    </Link>
                  </Span>
                </li>
              </ul>
            </div>
            <div className="menu-footer">
              <Span>
                <IoIosContacts />

                <Link to="/contacts">&nbsp; Contate-nos</Link>
              </Span>
              <button className="logout-button" onClick={() => logout()}>
                <IoIosLogOut />
                &nbsp; Logout
              </button>
            </div>
          </Menu>
        </>
      ) : (
        <div className="menu-closed" onMouseEnter={() => setToggle(true)}>
          <div className="menu-closed-mobile">
            <MenuOpenButton onClick={() => setToggle(true)}>
              <IoIosMenu />
            </MenuOpenButton>
          </div>
          <MenuDesktop>
            <ul className="dashboard-menu-itens">
              <li>
                <Span>
                  <IoMdPerson />
                </Span>
              </li>
              <li>
                <Span>
                  <IoIosBody />
                </Span>
              </li>

              <li>
                <Span>
                  <IoIosPeople />
                </Span>
              </li>
              <hr className="solid" />
              <li>
                <Span>
                  <IoMdPeople />
                </Span>
              </li>
              <li>
                <Span>
                  <IoIosSearch />
                </Span>
              </li>
            </ul>
            <div className="menu-footer">
              <Span>
                <IoIosContacts />
              </Span>
              <Span>
                <IoIosLogOut />
              </Span>
            </div>
          </MenuDesktop>
        </div>
      )}
    </>
  );
};

export default AppBar;
